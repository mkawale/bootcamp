# The name of the package
ATLAS_SUBDIR(AnalysisPayload)

find_package(ROOT COMPONENTS Gpad)

ATLAS_ADD_EXECUTABLE ( AnalysisPayload
                       util/AnalysisPayload.cxx
		  PUBLIC_HEADERS AnalysisPayload
		  LINK_LIBRARIES ${ROOT_LIBRARIES} 
                                 xAODEventInfo
				 xAODRootAccess
				 xAODJet
                                 JetSelectionHelperLib
				 AsgTools
                                 JetCalibToolsLib)
